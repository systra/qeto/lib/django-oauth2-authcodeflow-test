from django.shortcuts import render


def index_view(request):
    return render(request, 'index.html')


def fail_view(request):
    return render(request, 'fail.html')
