from django.urls import path
from . import views

urlpatterns = [
    path('', views.index_view, name='index'),
    path('next', views.index_view, name='next'),
    path('fail', views.fail_view, name='fail'),
]
